<?php $url = "http://" . $_SERVER['SERVER_NAME'] . '/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MeVest</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-10 col-xs-10">
                <a class="logo" href="/">
                    <img src="/images/logo.png" alt=""/>
                </a>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-2 col-xs-2 text-right">
                <span class="menu-mobile hidden-lg hidden-md">
                    <i class="fa fa-align-justify ico-mobile"></i>
                    <i class="fa fa-times ico-close"></i>
                </span>
                <div class="menu-desktop hidden-sm hidden-xs">
                    <ul>
                        <li>
                            <a href="">
                                About
                            </a>
                        </li>
                        <li class="active">
                            <div class="dropdown">
                                    <span id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Products & Services <i class="fa fa-angle-down"></i>
                                    </span>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dLabel">
                                    <li>
                                        <a href="#">
                                            Money Coaching
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Financial Plan
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Books
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="">
                                Employers
                            </a>
                        </li>
                        <li>
                            <a href="">
                                eLearning
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<nav class="mobile" data-status="0">
    <div class="row">
        <div class="col-lg-12">
            <ul>
                <li>
                    <a href="#">
                        About
                    </a>
                </li>
                <li class="active">
                    <a href="">
                        Products & Services
                    </a>
                </li>
                <li>
                    <a href="#">
                        Employers
                    </a>
                </li>
                <li>
                    <a href="">
                        eLearning
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>