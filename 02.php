<?php include('header.php'); ?>

    <div class="container-full">
        <div class="top-image mevest">
            <div class="text">
                MeVest™ Money Plan
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <article>
            <div class="row">
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/7.jpg&w=504&h=409&q=70" alt="Overview" />
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <h2>
                            Overview
                        </h2>
                    </div>
                    <div class="row-fluid description">
                        In partnership with the Debt Annihilators, MeVest brings you two signature financial transformation events in 2016: Get The Hell Out of Debt and
                        <a href="#">Get Rich and Stay Rich.</a> Albertans can gather with peers and friends in 10 collaborative and engaging sessions over 12 months. You’ll learn practical skills from our award-winning trainers to revolutionize your personal finances so you can kick-ass with your money and life. The best part is we won’t sell you products!
                    </div>
                </div>
            </div>
        </article>

    <article>
        <div class="row-fluid">
            <div class="sliver">

                <div class="visible-md-block visible-sm-block visible-xs-block col-md-12 col-sm-12 col-xs-12">
                    <div class="row-fluid">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/8.jpg&w=504&h=409&q=70" alt="Get The Hell Out of Debt" />
                    </div>
                </div>

                <div class="col-lg-5 col-lg-offset-2">
                    <div class="row-fluid">
                        <h2>
                            <span>Course 1</span> Get The Hell Out of Debt
                        </h2>
                    </div>
                    <div class="row-fluid description">
                        Struggling with debt? Can’t stop overspending? Debt is a mind matter and we’ll show you how to master it. You’ll crush your debt, and never rack up your credit card again. We do not consolidate debt and there is no quick-fix. You do the work with confidence and we’ll show you the way. Most clients are consumer debt free in 18-months.
                    </div>
                    <div class="row-fluid mtop20">
                        <a class="app-btn medium green" href="#">
                            Read more
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 hidden-md hidden-sm hidden-xs">
                    <div class="row">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/8.jpg&w=504&h=409&q=70" alt="Get The Hell Out of Debt" />
                    </div>
                </div>
            </div>
        </div>
    </article>
        <article>
            <div class="row">
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/9.jpg&w=504&h=409&q=70" alt="How It Works" />
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <h2>
                            <span>Course 2</span> Get Rich and Stay Rich
                        </h2>
                    </div>
                    <div class="row-fluid description">
                        Do you want to build wealth? We’ll teach you the building blocks of wealth creation and you don’t need to trade currency or rely on stock tips from friends. You’ll implement tried-and-true investing principles to make your money grow. This isn’t a get-rich-quick-scheme and we don’t provide investment advice. Most clients grow their wealth by tens of thousands of dollars in 18-months.
                    </div>
                    <div class="row-fluid mtop20">
                        <a class="app-btn medium green" href="#">
                            Read more
                        </a>
                    </div>
                </div>
            </div>
        </article>

        <article>
            <div class="row-fluid">
                <div class="sliver">


                <div class="visible-md-block visible-sm-block visible-xs-block col-md-12 col-sm-12 col-xs-12">
                    <div class="row-fluid">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/10.jpg&w=504&h=409&q=70" alt="Get Your Questions Answered" />
                    </div>
                </div>

                <div class="col-lg-5 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
                    <div class="row-fluid">
                        <h2 class="faq line">
                            <span>FAQ</span> Get Your Questions Answered
                        </h2>
                    </div>
                    <div class="row-fluid description">
                        <ul>
                            <li>
                                <div class="question">
                                    I want to take both courses. Can I?
                                </div>
                                <div class="answer">
                                    We recommend completing one course at a time.
                                </div>
                            </li>
                            <li>
                                <div class="question">
                                    I still have debt but want to take Get Rich Stay Rich. Should I?
                                </div>
                                <div class="answer">
                                    We recommend registering in Get the Hell Out of Debt if you are still carrying consumer debt (non-mortgage debt).
                                </div>
                            </li>
                            <li>
                                <div class="question">
                                    When will the courses be available where I live?
                                </div>
                                <div class="answer">
                                    We will complete our pilot program  in June 2016 and will announce plans for additional courses in new locations at that time. In the meantime, MeVest offers other services such as <a href="#">Money Coaching</a> and <a href="#">Money Master Plan</a>Money Master Plan. We also have a free newsletter with tips to be rich and happy.
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 hidden-md hidden-sm hidden-xs">
                    <div class="row">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/10.jpg&w=504&h=409&q=70" alt="Get Your Questions Answered" />
                    </div>
                </div>
            </div>
        </div>
    </article>
        <article>
            <div class="row">
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/11.jpg&w=504&h=409&q=70" alt="How It Works" />
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="green-title">
                        Logistics and Tid-bits
                    </div>
                    <div class="row-fluid">
                        <h2 class="logistics">
                            Dates 2016
                        </h2>
                    </div>
                    <div class="row-fluid description">
                        Here are the dates for your 10 sessions. Each course is offered on these dates:
                        <div class="calendar">
                            <ul>
                                <li>
                                    <strong>Calgary:</strong> January 3, February 7, March 6, April 10, May 1, June 12, September 11, October 2, November 6, December 1
                                </li>
                                <li>
                                    <strong>Edmonton&Red Deer:</strong> January 2, February 6, March 5, April 9, April 30, June 11, September 10, October 1, November 5, November 30
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </article>


    <article>
        <div class="row-fluid">
            <div class="sliver">


                    <div class="visible-md-block visible-sm-block visible-xs-block col-md-12 col-sm-12 col-xs-12">
                        <div class="row-fluid">
                            <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/12.jpg&w=504&h=409&q=70" alt="Get The Hell Out of Debt" />
                        </div>
                    </div>

                    <div class="col-lg-5 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
                        <div class="row-fluid">
                            <h2 class="first">
                                Capacity
                            </h2>
                        </div>
                        <div class="row-fluid description">
                            We can only accommodate 30 people per session on a first-come-first-serve basis.
                        </div>
                        <div class="row-fluid">
                            <h2 class="second">
                                About Debt Annihilators & MeVest
                            </h2>
                        </div>
                        <div class="row-fluid description">
                            Erin Skye Kelly & Jenn Widney have been helping people crush debt through simple strategies for decades. To date, participants have paid off millions of dollars in consumer debt. MeVest offers unbiased leading-edge financial education through eLearning, workshops, coaching and workplace training. Clients improve their net worth by $1,500/month!
                        </div>
                    </div>
                    <div class="col-lg-5 hidden-md hidden-sm hidden-xs">
                        <div class="row">
                            <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/12.jpg&w=504&h=409&q=70" alt="Get The Hell Out of Debt" />
                        </div>
                    </div>

             </div>
        </div>
    </article>
        <div class="pricing-column">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="pricing">
                        Pricing
                    </h2>
                </div>
            </div>
            <div class="row price">
                <div class="col-lg-2 col-lg-offset-4 text-center">
                    <a href="#">
                        <div class="row">
                            <div class="big-prace">
                                <span>$</span>770
                            </div>
                        </div>
                        <div class="row prace-title">
                            Early Bird
                            <span>
                                (until December 15)*
                            </span>
                        </div>
                        <div class="row info">
                            <span class="line"></span>
                            *You'll receive a 30-minute bonus coaching call and welcome package.
                        </div>
                    </a>
                </div>
                <div class="col-lg-2 text-center">
                    <a href="#">
                        <div class="row">
                            <div class="big-prace">
                                <span>$</span>1,200
                            </div>
                        </div>
                        <div class="row prace-title">
                            Regular
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-full">
        <div class="bg-ready">
            <div class="row-fluid text-center">
                <div class="title">
                    Are You Ready To Start?
                </div>
            </div>
            <div class="row-fluid text-center">
                <div class="entry">
                    Let's transform your life through MeVest, starting right now.
                </div>
            </div>
            <div class="row-fluid text-center mtop40">
                <a class="app-btn medium green" href="#">
                    Get The Hell Out of Debt
                </a>
                <a class="app-btn medium green double" href="#">
                    Get Rich. Stay Rich.
                </a>
            </div>
        </div>
    </div>



    <div class="container-full">
        <div class="bg-ready live">
            <div class="row-fluid text-center">
                <div class="col-lg-12">
                    <div class="title">
                        Live somewhere else in Canada?
                    </div>
                </div>
            </div>
            <div class="row-fluid text-center">
                <div class="col-lg-12">
                    <div class="info">
                        If so, there are still services for you. We’re piloting our programs in major centres over the next 18 months
                    </div>
                    <div class="line"></div>
                </div>

            </div>
            <div class="row-fluid text-center">
                <div class="col-lg-12">
                    <div class="entry">
                        Sign-up for our monthly eNews and you'll be the first to know about upcoming events in other cities when they become available.
                    </div>
                </div>

            </div>
            <form action="">
                <div class="row-fluid">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="row-fluid">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mtop20">
                                <input class="form-control" type="text" placeholder="Your name" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mtop20">
                                <input class="form-control" type="email" placeholder="Email address" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid text-center">
                    <div class="col-lg-12 mtop20">
                        <button class="app-btn medium dark-green" type="submit">
                            Submit <i class="fa fa-caret-right"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-lg-12 text-center">
                <div class="address-info">
                    <ul>
                        <li>
                            <span>Phone</span>
                            403-831-8576
                        </li>
                        <li>
                            <span>Email</span>
                            <a href="mailto:info@mevest.ca">
                                info@MeVest.ca
                            </a>
                        </li>
                        <li>
                            <span>Web</span>
                            <a href="http://www.mevest.ca" target="_blank">
                                www.MeVest.ca
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>