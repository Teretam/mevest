    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <a class="logo" href="/">
                        <img src="/images/logo.png" alt=""/>
                    </a>
                </div>


                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 col-lg-offset-1">
                    <div class="row widget-area">
                        <ul>
                            <li class="title">
                                Explore:
                            </li>
                            <li>
                                <a href="">
                                    About
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    Blog
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    Coaching
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    Employers
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    Elearning
                                </a>
                            </li>
                        </ul>
                        <ul>
                            <li class="title">
                                Also visit:
                            </li>
                            <li>
                                <a href="http://lesleyscorgie.com" target="_blank">
                                    Lesleyscorgie.com
                                </a>
                            </li>
                        </ul>
                        <ul>
                            <li class="title">
                                Get in touch:
                            </li>
                            <li>
                                <a href="mailto:info@mevest.ca" target="_blank">
                                    info@mevest.ca
                                </a>
                            </li>
                            <li>
                                <a href="http://www.twitter.com/MeVestMoney">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="http://www.facebook.com/">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="http://www.linkedin.com/">
                                    <i class="fa fa-linkedin-square"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 rights">
                    Copyright 2014, Rich by Inc. <a href="http://vordik.com/" target="_blank">Website by Vordik.</a>
                </div>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>
</body>
</html>