<div class="container-full">
    <div class="top-image">
        <div class="text">
            Money Coaching
        </div>
    </div>
</div>
<div class="container-fluid">
    <article>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="row">
                    <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/1.jpg&w=504&h=409&q=70" alt="Get Financially Fit" />
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="row-fluid">
                    <h2>
                        Get Financially Fit
                    </h2>
                </div>
                <div class="row-fluid entry">
                    Financial success comes to those who work for it.
                </div>
                <div class="row-fluid description">
                    MeVest is a personal trainer for your money. Over 12-months we dissect your finances, identify barriers preventing you from building wealth, and give you life skills to overcome them. Our program is custom fit to your needs, financial experience and income. The harder you work with us, the more money you’ll make.
                </div>
            </div>
        </div>
    </article>
    <article>
        <div class="row">
            <div class="sliver">
                <!-- col-lg-offset-2  -->
                <div class="visible-sm-block visible-xs-block col-sm-12 col-xs-12">
                    <div class="row>
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/2.jpg&w=504&h=409&q=70" alt="Improve Your Net Worth" />
                    </div>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-md-5 col-sm-12 col-xs-12">
                    <div class="row-fluid">
                        <h2>
                            Improve Your Net Worth
                        </h2>
                    </div>
                    <div class="row-fluid entry">
                        Crush debt. Build assets. Grow Income.
                    </div>
                    <div class="row-fluid description">
                        You will transform your net worth through our debt-reduction system called “Crush-It”, and you’ll never rack up debt again. You’ll learn how to invest like the pros and make money by respecting your risk tolerance. You’ll build skills to successfully navigate difficult financial conversations with your partner, break bad habits and grow savings for emergencies. We’ll also show you how to get a raise, start a business and generate passive income. Most importantly, we’ll design a financial plan to support your life’s next adventure.
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
                    <div class="row">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/2.jpg&w=504&h=409&q=70" alt="Improve Your Net Worth" />
                    </div>
                </div>
            </div>


        </div>
    </article>
    <article>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="row">
                    <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/3.jpg&w=504&h=409&q=70" alt="How It Works" />
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="row-fluid">
                    <h2 class="sub">
                        How It Works
                    </h2>
                </div>
                <div class="row-fluid entry">
                    FVirtual or in-person
                </div>
                <div class="row-fluid description">
                    We’ve got your busy schedule covered. You’ll meet with your coach once per month for one hour. You pick the time; any day of the week, or hour of the day. Up to five action items will come out of every meeting, which you and your coach will work through until you meet again, one month later. These action items take between 3 and 5 hours of effort every month. Not into homework? You shouldn’t sign up. Financial fitness takes work.
                </div>
                <div class="row-fluid">
                    <small>
                        *Our money coaches are located in most major centres. If you live in a location where we don’t have a MeVest Certified Money Coach, your program will be virtual.
                    </small>
                </div>
                <div class="row-fluid">
                    <div class="small-title">
                        It’s Time To Make Sense of Your Money
                    </div>
                </div>
            </div>
        </div>
    </article>

    <div class="row">
        <div class="know-image">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-lg-offset-2">
                <div class="question">
                    Did You Know?
                </div>
                <div class="answer">
                    Our clients average a net worth improvement of $1,500 per month.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <h2 class="pricing">
                Pricing
            </h2>
        </div>
    </div>
    <div class="row pricing">
        <div class="col-lg-2 col-lg-offset-4 text-center">
            <a href="#">
                <div class="row">
                    <img src="/images/icon-couple.png" alt="Couple"/>
                </div>
                <div class="row">
                    <div class="title">
                        Couples
                    </div>
                </div>
                <div class="row">
                    <div class="price">
                        $200/month
                    </div>
                </div>
            </a>
        </div>
        <div class="col-lg-2 text-center">
            <a href="#">
                <div class="row">
                    <img src="/images/icon-man.png" alt="Couple"/>
                </div>
                <div class="row">
                    <div class="title">
                        Singles
                    </div>
                </div>
                <div class="row">
                    <div class="price">
                        $175/month
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="bg-ready">
        <div class="row-fluid text-center">
            <div class="title">
                Are You Ready To Start?
            </div>
        </div>
        <div class="row-fluid text-center">
            <div class="entry">
                Let's transform your life through MeVest, starting right now.
            </div>
        </div>
        <div class="row-fluid text-center mtop40">
            <a class="app-btn medium green" href="#">
                Start Now!
            </a>
        </div>
    </div>
</div>