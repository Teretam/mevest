$('.menu-mobile').click(function() {
    var status = $('.mobile').attr('data-status');
    if (status == 0) {
        $("body").addClass('bodyMenuOpen');
        $(".mobile, header").on('touchmove', function(e) {
            if(status == 0) {
                e.preventDefault();
            }
        });
        $('.ico-mobile').hide();
        $('.ico-close').show();
        $('.mobile').attr('data-status', 1).addClass('active').show();
        $('body').css('display', 'block');
        $('.container-index-box, .container-page, footer').hide();

    } else if (status == 1) {
        $('.ico-close').hide();
        $('.ico-mobile').show();
        $("body").removeClass('bodyMenuOpen');
        $('.mobile').hide().attr('data-status', 0);
        $('.container-index-box, .container-page, footer').show();
        $('body').css('display', 'block');
    }
});