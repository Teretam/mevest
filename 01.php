<?php include('header.php'); ?>

    <div class="container-full">
        <div class="top-image master">
            <div class="text">
                Money Master Plan
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <article>
            <div class="row">
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/4.jpg&w=504&h=409&q=70" alt="A Master Plan for Your Money" />
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="row-fluid">
                        <h2>
                            A Master Plan for Your Money
                        </h2>
                    </div>
                    <div class="row-fluid entry">
                        A goal without a plan is only a wish
                    </div>
                    <div class="row-fluid description">
                        If you want to make financial progress, you need a written plan for your money. Our friendly and experienced planners will help you get laser focused on your top three financial goals, while providing step-by-step strategies on how to achieve them.
                    </div>
                </div>
            </div>
        </article>


        <article>
            <div class="row-fluid">
                <div class="sliver">

                    <div class="visible-sm-block visible-xs-block col-sm-12 col-xs-12">
                        <div class="row>
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/2.jpg&w=504&h=409&q=70" alt="Improve Your Net Worth" />
                    </div>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-md-5 col-sm-12 col-xs-12">
                    <div class="row-fluid">
                        <h2>
                            Build Wealth Your Way
                        </h2>
                    </div>
                    <div class="row-fluid entry">
                        Be your own kind of rich
                    </div>
                    <div class="row-fluid description">
                        What does your future hold? A vacation property? Sending your children to college? Paying off your mortgage? Starting a business? Doubling your income? We’ll dig into your hopes and dreams for the future and bring you face to face with the financial barriers that will prevent you from making your dreams a reality. Your Money Master Plan will arm you with practical strategies to overcome these challenges and grow your wealth.
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 hidden-sm hidden-xs">
                    <div class="row">
                        <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/5.jpg&w=504&h=409&q=70" alt="Improve Your Net Worth" />
                    </div>
                </div>
            </div>
        </article>


    <article>
        <div class="row">
            <div class="col-lg-5">
                <div class="row-fluid">
                    <img class="img-responsive" src="<?php echo $url; ?>timthumb.php?src=<?php echo $url; ?>images/example/6.jpg&w=504&h=409&q=70" alt="How It Works" />
                </div>
            </div>
            <div class="col-lg-5">
                <div class="row-fluid">
                    <h2>
                        How It Works
                    </h2>
                </div>
                <div class="row-fluid entry">
                    You design it. We build it. You make it happen.
                </div>
                <div class="row-fluid description">
                    We get straight to the point with Money Master Plan. Using our simple templates, you’ll tell us your net worth, budget and top three goals for the next five years. We’ll design a comprehensive plan, with strategies, to get you there. No, we will not bury you in paperwork. We’ll deliver you a no-nonsense 4-page plan. This virtual experience is private and when your plan is ready, your planner will set up a 30-minute call to answer your questions. Then it’s up to you to execute.
                </div>
            </div>
        </div>
    </article>

    <article>
        <div class="pricing-silver">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="pricing">
                        Pricing
                    </h2>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-lg-4 col-lg-offset-4 text-center">
                    <a href="#">
                        <div class="big-prace">
                            <span>$</span>399
                        </div>
                    </a>
                </div>
            </div>
            <div class="row pricing">
                <div class="col-lg-12 text-center">
                    <h3 class="pricing">
                        It’s Time To Make Sense of Your Money
                    </h3>
                </div>
            </div>
        </div>
    </div>
    </article>




    <div class="container-full">
        <div class="bg-ready">
            <div class="row-fluid text-center">
                <div class="title">
                    Are You Ready To Start?
                </div>
            </div>
            <div class="row-fluid text-center">
                <div class="entry">
                    Let's transform your life through MeVest, starting right now.
                </div>
            </div>
            <div class="row-fluid text-center mtop40">
                <a class="app-btn medium green" href="#">
                    Start Now!
                </a>
            </div>
        </div>
    </div>
<?php include('footer.php'); ?>