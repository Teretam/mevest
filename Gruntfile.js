module.exports = function(grunt) {

    // Load task build time
    require('time-grunt')(grunt);

    // Application config.
    var app = {
        app: {
            path: '.'
        },
        tmp: {
            path: '.tmp'
        }
    };

    // Grunt config
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Load app object
        app: app,

        // Watch file
        watch: {
            options: {
                livereload: true
            },
            base: {
                files: [
                    'Gruntfile.js',
                    '<%= app.app.path %>/js/**/*.js',
                    '<%= app.app.path %>/less/**/*.less',
                    '<%= app.app.path %>/*.php'
                ],
                tasks: ['build']
            }
        },        

        // Compile LESS files to CSS and copy to folder
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    // Destination file || Source file
                    "<%= app.app.path %>/style.css": '<%= app.app.path %>/less/app.less'
                }
            }
        },

        /*
        // Minify CSS
        cssmin: {
            files: {
                // Destination file || Source file
                '<%= app.app.path %>/style.css': '<%= app.app.path %>/style.css'
            }
        },
*/
        cssmin: {
            target: {
                files: {
                    '<%= app.app.path %>/style.css': '<%= app.app.path %>/style.css'
                }
            }
        }



    });

    // Load task
    grunt.loadNpmTasks('grunt-contrib-watch');      // Watch files
    grunt.loadNpmTasks('grunt-contrib-less');       // Compile LESS files to CSS
    //grunt.loadNpmTasks('grunt-contrib-clean');      // Clear folder/files
    grunt.loadNpmTasks('grunt-contrib-cssmin');     // Minify CSS
    grunt.loadNpmTasks('grunt-contrib-less');       // Compile LESS files to CSS
    //grunt.loadNpmTasks('grunt-usemin');
    //grunt.loadNpmTasks('grunt-contrib-htmlmin');

    // Run task
    grunt.registerTask('build', [
        'less',
        //'useminPrepare',
        //'concat',
        //'uglify',
        //'usemin',
        'cssmin'
        //'clean:tmp'
    ]);

    // Load grunt task, watch
    grunt.registerTask('default', ['watch']);

};

